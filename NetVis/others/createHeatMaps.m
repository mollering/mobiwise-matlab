clearvars
%% Main Options

chosenHarvesting = 2;
chosenAlpha = [5];

chosenOption = 7;
chosenTslot = 60;

%% Graphic Options
% 1: Sensing HeatMap
%    Show Cumulative sum until certain TimeSlot of Number of Sensings
%    Chosen Time Slot can be changed by 'chosenTslot' var
%           If chosenTslot is 120, then it shows the cumulative sum from
%           all the Simulation


% 2: Sleeping HeatMap
%    Show Cumulative sum until certain TimeSlot of Number of Sleepings
%    Chosen Time Slot can be changed by 'chosenTslot' var
%           If chosenTslot is 120, then it shows the cumulative sum from
%           all the Simulation


% 3: Tx HeatMap
%    Show Cumulative sum until certain TimeSlot of Number of Tx's
%    Chosen Time Slot can be changed by 'chosenTslot' var
%           If chosenTslot is 120, then it shows the cumulative sum from
%           all the Simulation

% 4: Rx HeatMap
%    Show Cumulative sum until certain TimeSlot of Number of Rx's
%    Chosen Time Slot can be changed by 'chosenTslot' var
%           If chosenTslot is 120, then it shows the cumulative sum from
%           all the Simulation

% 5: Node Activity HeatMap
%    Show Cumulative sum of Spent Battery until certain TimeSlot
%    Chosen Time Slot can be changed by 'chosenTslot' var
%           If chosenTslot is 120, then it shows the cumulative sum from
%           all the Simulation

% 6: Edge Activity HeatMap
%    Show Cumulative sum of Payloads crossing Edges until certain TimeSlot
%    Chosen Time Slot can be changed by 'chosenTslot' var
%           If chosenTslot is 120, then it shows the cumulative sum from
%           all the Simulation

% 7: Payloads Left On Nodes HeatMap
%    Show Total Payloads left on the Nodes at the end of the simulation
%    Chosen Time Slot can be changed by 'chosenTslot' var
%           If chosenTslot is 120, then it shows the cumulative sum from
%           all the Simulation

% 8: Distance to Sink
%    Show Distance to Sink


%% Plots
for H = chosenHarvesting
    for A = chosenAlpha
        
        uiopen('load');
        
        fig = figure;
        fig.NumberTitle = 'off';
        ax = axes(fig);
        hold(ax, 'on');
        colormap cool
        
        ax.Units = 'normalized';
        
        p = plot(ax, ...
            NVDat.NetInfo.Graph, ...
            'XData', NVDat.NetInfo.Graph.Nodes.XYCoords(:,1), ...
            'YData', NVDat.NetInfo.Graph.Nodes.XYCoords(:,2), ...
            'Marker', 'o', ...
            'MarkerSize',  11*ones(1, NVDat.NetInfo.NumberOfNodes), ...
            'LineStyle', '-', ...
            'NodeCData', ones(1, NVDat.NetInfo.NumberOfNodes), ...
            'EdgeCData', ones(1, NVDat.NetInfo.NumberOfEdges), ...
            'NodeColor', 'flat', ...
            'EdgeColor', 'flat', ...
            'NodeLabel', [] ...
            );
        % Sink Node
        scatter(ax, ...
            NVDat.NetInfo.Graph.Nodes.XYCoords(NVDat.NetInfo.SinkNode,1), ...
            NVDat.NetInfo.Graph.Nodes.XYCoords(NVDat.NetInfo.SinkNode,2), ...
            170, ...
            'k', 'filled');
        
        ax.Visible = 'off';
        
        
        
        switch chosenOption
            case 1
                Option = 'Sensing';
                
                t = chosenTslot + 1;
                
                % Color Data
                p.NodeCData = NVDat.SimResults.Network(t).NodesCumulative.SensingCount;
                ax.CLim = ...
                    [min(NVDat.SimResults.Network(t).NodesCumulative.SensingCount(~NVDat.NetInfo.Graph.Nodes.IsSink)) ...
                    max(NVDat.SimResults.Network(t).NodesCumulative.SensingCount(~NVDat.NetInfo.Graph.Nodes.IsSink))];
                c = colorbar(ax);
                c.Label.String = 'Sensing Counter';
                
                % Node Labels
                p.NodeLabel = ...
                    arrayfun(@num2str, ...
                    NVDat.SimResults.Network(t).NodesCumulative.SensingCount, ...
                    'UniformOutput', false);
                p.NodeLabel{NVDat.NetInfo.Graph.Nodes.IsSink} = '';
                
                % UI Settings
                axis vis3d
                
                fig.Name = [sprintf('Cumulative Sensing until timeslot %d', chosenTslot), ...
                    ' | H ', num2str(H), ' | alpha ', num2str(A)];
                
            case 2
                Option = 'Sleeping';
                
                t = chosenTslot + 1;
                
                % Color Data
                p.NodeCData = NVDat.SimResults.Network(t).NodesCumulative.SleepingCount;
                ax.CLim = ...
                    [min(NVDat.SimResults.Network(t).NodesCumulative.SleepingCount(~NVDat.NetInfo.Graph.Nodes.IsSink)) ...
                    max(NVDat.SimResults.Network(t).NodesCumulative.SleepingCount(~NVDat.NetInfo.Graph.Nodes.IsSink))];
                c = colorbar(ax);
                c.Label.String = 'Sleeping Counter';
                
                % Node Labels
                p.NodeLabel = ...
                    arrayfun(@num2str, ...
                    NVDat.SimResults.Network(t).NodesCumulative.SleepingCount, ...
                    'UniformOutput', false);
                p.NodeLabel{NVDat.NetInfo.Graph.Nodes.IsSink} = '';
                
                % UI Settings
                axis vis3d
                
                fig.Name = [sprintf('Cumulative Sleeping until timeslot %d', chosenTslot), ...
                    ' | H ', num2str(H), ' | alpha ', num2str(A)];
                
            case 3
                Option = 'Tx';
                
                t = chosenTslot + 1;
                
                % Color Data
                p.NodeCData = NVDat.SimResults.Network(t).NodesCumulative.TxCount;
                ax.CLim = ...
                    [min(NVDat.SimResults.Network(t).NodesCumulative.TxCount(~NVDat.NetInfo.Graph.Nodes.IsSink)) ...
                    max(NVDat.SimResults.Network(t).NodesCumulative.TxCount(~NVDat.NetInfo.Graph.Nodes.IsSink))];
                c = colorbar(ax);
                c.Label.String = 'Tx Counter';
                
                % Node Labels
                p.NodeLabel = ...
                    arrayfun(@num2str, ...
                    NVDat.SimResults.Network(t).NodesCumulative.TxCount, ...
                    'UniformOutput', false);
                p.NodeLabel{NVDat.NetInfo.Graph.Nodes.IsSink} = '';
                
                % UI Settings
                axis vis3d
                
                fig.Name = [sprintf('Cumulative Tx until timeslot %d', chosenTslot), ...
                    ' | H ', num2str(H), ' | alpha ', num2str(A)];
                
            case 4
                Option = 'Tx';
                
                t = chosenTslot + 1;
                
                % Color Data
                p.NodeCData = NVDat.SimResults.Network(t).NodesCumulative.RxCount;
                ax.CLim = ...
                    [min(NVDat.SimResults.Network(t).NodesCumulative.RxCount(~NVDat.NetInfo.Graph.Nodes.IsSink)) ...
                    max(NVDat.SimResults.Network(t).NodesCumulative.RxCount(~NVDat.NetInfo.Graph.Nodes.IsSink))];
                c = colorbar(ax);
                c.Label.String = 'Rx Counter';
                
                % Node Labels
                p.NodeLabel = ...
                    arrayfun(@num2str, ...
                    NVDat.SimResults.Network(t).NodesCumulative.RxCount, ...
                    'UniformOutput', false);
                p.NodeLabel{NVDat.NetInfo.Graph.Nodes.IsSink} = '';
                
                % UI Settings
                axis vis3d
                
                fig.Name = [sprintf('Cumulative Rx until timeslot %d', chosenTslot), ...
                    ' | H ', num2str(H), ' | alpha ', num2str(A)];
                
            case 5
                Option = 'Node Activity';
                
                t = chosenTslot + 1;
                
                % Color Data
                p.NodeCData = NVDat.SimResults.Network(t).NodesCumulative.EnergyOut;
                ax.CLim = ...
                    [min(NVDat.SimResults.Network(t).NodesCumulative.EnergyOut(~NVDat.NetInfo.Graph.Nodes.IsSink)) ...
                    max(NVDat.SimResults.Network(t).NodesCumulative.EnergyOut(~NVDat.NetInfo.Graph.Nodes.IsSink))];
                c = colorbar(ax);
                c.Label.String = 'Activity (Spent % Battery)';
                
               
                % UI Settings
                axis vis3d
                
                fig.Name = [sprintf('Activity on Nodes until timeslot %d', chosenTslot), ...
                    ' | H ', num2str(H), ' | alpha ', num2str(A)];
                
            case 6
                                
                tEnd = chosenTslot + 1;
                
                PayloadsOnEdgesCount = zeros(NVDat.NetInfo.NumberOfEdges, 1);
                for t = 2:tEnd
                        for e = 1:NVDat.SimResults.Network(t).Totals.NumberOfActiveEdges
                            PayloadsOnEdgesCount(NVDat.SimResults.Network(t).ActiveEdges.EdgeIDs(e)) = ...
                                PayloadsOnEdgesCount(NVDat.SimResults.Network(t).ActiveEdges.EdgeIDs(e)) + ...
                                NVDat.SimResults.Network(t).ActiveEdges.NumberOfPayloads(e);
                        end
                end
                
                
                % Color Data
                p.EdgeCData = PayloadsOnEdgesCount;
                ax.CLim = ...
                    [min(PayloadsOnEdgesCount) ...
                    max(PayloadsOnEdgesCount)];
                c = colorbar(ax);
                c.Label.String = 'Activity (# Payloads)';
                colormap jet
                
                p.NodeColor = [59, 61, 66]/255;
                p.LineWidth = 7;
                
                % Edge Labels
                p.EdgeLabel = ...
                    arrayfun(@num2str, ...
                    PayloadsOnEdgesCount, ...
                    'UniformOutput', false);
                
                % UI Settings
                axis vis3d
                fig.Name = [sprintf('Activity on Edges until timeslot %d', chosenTslot), ...
                    ' | H ', num2str(H), ' | alpha ', num2str(A)];
                
            case 7
                
                % Color Data
                p.NodeCData = ...
                    NVDat.SimResults.Network(NVDat.NetInfo.TimeFrame+1).Nodes.PayloadsEnd;
                ax.CLim = ...
                    [min(NVDat.SimResults.Network(NVDat.NetInfo.TimeFrame+1).Nodes.PayloadsEnd(~NVDat.NetInfo.Graph.Nodes.IsSink)) ...
                    max(NVDat.SimResults.Network(NVDat.NetInfo.TimeFrame+1).Nodes.PayloadsEnd(~NVDat.NetInfo.Graph.Nodes.IsSink))];
                c = colorbar(ax);
                c.Label.String = '# Payloads left on Nodes';
                colormap jet
                
                               
                % Node Labels
                p.NodeLabel = ...
                    arrayfun(@num2str, ...
                    NVDat.SimResults.Network(NVDat.NetInfo.TimeFrame+1).Nodes.PayloadsEnd, ...
                    'UniformOutput', false);
                
                % UI Settings
                axis vis3d
                fig.Name = ['Payloads left on Nodes at the End of Simulation', ...
                    ' | H ', num2str(H), ' | alpha ', num2str(A)];
                
            case 8
                
                % Color Data
                p.NodeCData = NVDat.NetInfo.Graph.Nodes.DistanceToSink;
                c = colorbar(ax);
                c.Label.String = 'Distance to Sink Node';
                colormap jet
                
                
                % Node Labels
                p.NodeLabel = ...
                    arrayfun(@num2str, ...
                    NVDat.NetInfo.Graph.Nodes.DistanceToSink, ...
                    'UniformOutput', false);
                p.NodeLabel{NVDat.NetInfo.Graph.Nodes.IsSink} = '';
                
                % UI Settings
                axis vis3d
                fig.Name = ['Distance to Sink Node', ...
                    ' | H ', num2str(H), ' | alpha ', num2str(A)];
        end
    end
end