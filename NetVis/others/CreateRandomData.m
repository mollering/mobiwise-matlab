
T = 3;
N = 6;

StateNames = [
    "zzz";
    "Rx";
    "Tx";
    "s";
    "s";
    "zzz"
    ];
NumberOfStates = size(StateNames, 1);

NetworkState = cell(1, T);
for t = 1:T
    NetworkState{t} = table((1:N)', ...
        StateNames(randi(NumberOfStates, N, 1)), ...
        randi(10,N,1));
    NetworkState{t}.Properties.VariableNames = {'Node', 'State', 'Payloads'};
end

save('NetworkState.mat', 'NetworkState');

