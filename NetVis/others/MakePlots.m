
close all;
% N = 9
startSim = 1;
endSim   = 6;

i = 1;
for s = startSim:endSim
    
    fileName = sprintf('P%d.mat', s);
    load(fileName);
    NNodes(i)            = NVDat.NetInfo.NumberOfNodes;
    NTimeSlots(i)        = NVDat.NetInfo.TimeFrame;
    PayloadsAtSink(i)    = NVDat.SimResults.Network(end).Totals.NumberOfPayloadsAtSink;
    PayloadsAtNetwork(i) = NVDat.SimResults.Network(end).Totals.NumberOfPayloadsOutsideSink;

    i = i + 1;
    
end

p = plot(NTimeSlots, PayloadsAtSink, ...
    'DisplayName', sprintf('N = %d', NNodes(1)), ...
    'Marker', 'square', ...
    'MarkerSize', 5, ...
    'MarkerFaceColor', 'k');
legend('Location', 'best');
hold on;
grid on;
xlabel('TimeFrame');
ylabel('Total Number of Payloads that reached Sink Node');
title('Payloads at Sink Node vs TimeFrame');

% N = 12
startSim = 14;
endSim   = 19;

i = 1;
for s = startSim:endSim
    
    fileName = sprintf('P%d.mat', s);
    load(fileName);
    NNodes(i)            = NVDat.NetInfo.NumberOfNodes;
    NTimeSlots(i)        = NVDat.NetInfo.TimeFrame;
    PayloadsAtSink(i)    = NVDat.SimResults.Network(end).Totals.NumberOfPayloadsAtSink;
    PayloadsAtNetwork(i) = NVDat.SimResults.Network(end).Totals.NumberOfPayloadsOutsideSink;

    i = i + 1;
    
end

plot(NTimeSlots, PayloadsAtSink, ...
    'DisplayName', sprintf('N = %d', NNodes(1)), ...
    'Marker', 'square', ...
    'MarkerSize', 5, ...
    'MarkerFaceColor', 'k');


% N = 16
startSim = 27;
endSim   = 32;

i = 1;
for s = startSim:endSim
    
    fileName = sprintf('P%d.mat', s);
    load(fileName);
    NNodes(i)            = NVDat.NetInfo.NumberOfNodes;
    NTimeSlots(i)        = NVDat.NetInfo.TimeFrame;
    PayloadsAtSink(i)    = NVDat.SimResults.Network(end).Totals.NumberOfPayloadsAtSink;
    PayloadsAtNetwork(i) = NVDat.SimResults.Network(end).Totals.NumberOfPayloadsOutsideSink;

    i = i + 1;
    
end

plot(NTimeSlots, PayloadsAtSink, ...
    'DisplayName', sprintf('N = %d', NNodes(1)), ...
    'Marker', 'square', ...
    'MarkerSize', 5, ...
    'MarkerFaceColor', 'k');
