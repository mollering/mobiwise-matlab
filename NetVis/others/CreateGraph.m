

N = 10; % Number of points
MarkerSize = 15;
FontSize = MarkerSize-5;

minNeighbourDistance = 1.1; %between 1.1 and 5
NeighbourDistance = 1*minNeighbourDistance;

NodePoints = createPoints(N, minNeighbourDistance);
AdjMatrix = getAdjacencies(NodePoints, NeighbourDistance);
digraphG = graph(AdjMatrix);
graphG = graph(AdjMatrix);


SinkNode = randi(N);


GraphPlot = plot(digraphG, 'Marker', 'none');
hold on;
GraphPlot.XData = NodePoints(:,1)';
GraphPlot.YData = NodePoints(:,2)';
GraphPlot.MarkerSize = MarkerSize;
%GraphPlot.ArrowSize = ArrowSize;
GraphPlot.LineStyle = '-';
GraphPlot.NodeLabel = [];

sensing = [1 3 6 8];
sleeping = [2 7 9 10];
Tx = [4];
Rx = [5];
NodePlot(1) = line(NodePoints(sensing,1), NodePoints(sensing,2), 'LineStyle', 'none');
NodePlot(2) = line(NodePoints(sleeping,1), NodePoints(sleeping,2), 'LineStyle', 'none');
NodePlot(3) = line(NodePoints(Tx,1), NodePoints(Tx,2), 'LineStyle', 'none');
NodePlot(4) = line(NodePoints(Rx,1), NodePoints(Rx,2), 'LineStyle', 'none');

NodePlot(1).Marker = 'o';
NodePlot(2).Marker = '.';
NodePlot(3).Marker = '>';
NodePlot(4).Marker = '<';

hold off;


NodeLabels = arrayfun(@(n) {sprintf('%d', n)}, (1:N)');
NodeLabelsPlot = text(...
    NodePoints(:,1), NodePoints(:,2), ...
    NodeLabels, ...
   'FontWeight', 'bold', ...
   'FontSize', FontSize, ...
   'Color', 'white', ...
   'HorizontalAlignment', 'center', ...
   'VerticalAlignment', 'middle', ...
   'BackgroundColor', 'none');

NodeColorMap = [21/255, 89/255, 198/255  % 1.blue = regular node
    2/255, 24/255, 58/255                % 2.black = sink
    11/255, 96/255, 6/255];

colormap(NodeColorMap);
GraphPlot.NodeCData = ones(1,N);
%GraphPlot.EdgeCData = ones(1,numedges(G));
GraphPlot.NodeCData(SinkNode) = 0;

axis on
hz = zoom;
hz.ActionPostCallback = @mypostcallback;


hz.Enable = 'on';

function mypostcallback(obj,evt)
hz = zoom(evt.Axes);
factor = 2/3;
if (strcmp(hz.Direction, 'in'))
    factor = 1.5;
end
evt.Axes.Children(end).MarkerSize = evt.Axes.Children(end).MarkerSize*factor;
for i = 1:(size(evt.Axes.Children,1)-1)
    evt.Axes.Children(i).FontSize = evt.Axes.Children(i).FontSize*factor;
end
end


%NumObjects = numnodes(G);% + numedges(G);

% NetworkState = cell(1, T);
% 
% NodeState = ["zzz","Rx","Tx","s","s","zzz"]';
% NodeState(SinkNode) = "sink";
% NetworkState{1} = table((1:N)', NodeState, randi(10,N,1));
% NetworkState{1}.Properties.VariableNames = {'Node', 'State', 'Payloads'};
% 
% 
% % In an arbitrary time slot
% t = 1;
% G.Nodes.Payloads = NetworkState{t}.Payloads;
% G.Nodes.State = NetworkState{t}.State;
% 
% PayloadLabels = arrayfun(@(n) {sprintf('P: %d', n)}, G.Nodes.Payloads);
% PayloadLabelsPlot = text(NodePoints(:,1)+0.01, NodePoints(:,2)+0.01, PayloadLabels, ...
%    'FontWeight', 'bold', ...
%    'Color', 'black', ...
%    'HorizontalAlignment', 'left', ...
%    'VerticalAlignment', 'top', ...
%    'BackgroundColor', 'none');
% 
% StateLabels = arrayfun(@(n) {n}, G.Nodes.State);
% StateLabelsPlot = text(NodePoints(:,1)-0.01, NodePoints(:,2)-0.01, StateLabels, ...
%    'FontWeight', 'bold', ...
%    'Color', 'black', ...
%    'HorizontalAlignment', 'right', ...
%    'VerticalAlignment', 'cap', ...
%    'BackgroundColor', 'none');