function [ Points ] = createPoints( numberOfPoints, minNeighbourDistance )

minDistanceBetweenPoints = 1;
rng('shuffle');

% First set of 2D-coordinates
Points = rand(1,2);

counter = 1;
while (counter < numberOfPoints)
    % Create a random angle between 0 and 2pi
    randomAngle = 2*pi*rand(1);
    % Create a random distance between two given values
    randomDistance = minDistanceBetweenPoints + (minNeighbourDistance-minDistanceBetweenPoints)*rand(1);
    
    % Define a new point that has the random angle/distance from a
    % random point of the set of points previously created
    randomPoint = randi(counter);
    newPoint(1) = Points(randomPoint, 1) + randomDistance*cos(randomAngle);
    newPoint(2) = Points(randomPoint, 2) + randomDistance*sin(randomAngle);
    
    % Only accept that point if it isn't inside the radius of the other
    % points
    if (pdist2(Points, newPoint, 'euclidean', 'Smallest', 1) > minDistanceBetweenPoints)
        counter = counter + 1;
        Points(counter,:) = newPoint;
    end
end

end

