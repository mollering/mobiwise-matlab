function [ AdjacencyMatrix ] = getAdjacencies(NodePoints, NeighborRadii)



Distances = pdist2(NodePoints, NodePoints);
Distances(Distances > NeighborRadii) = 0;
AdjacencyMatrix = logical(Distances);

end

