clearvars
%% Main Options

chosenHarvesting = 2;
chosenAlpha = 5;

chosenOption = 1;
chosenTslot = 1;

%% Graphic Options
% 1: Node States Percentages of Total Time Bar Graph


%% Plots
for H = chosenHarvesting
    for A = chosenAlpha
        
        fileName = sprintf('U3', H, A);
        load(fileName);
        
        fig = figure;
        fig.NumberTitle = 'off';
        ax = axes(fig);
        hold(ax, 'on');
        
        
        ax.Units = 'normalized';

        
        tempTable = zeros(NVDat.NetInfo.NumberOfNodes, 5);
        tempTable(:,1) = NVDat.NetInfo.Graph.Nodes.DistanceToSink;
        for t = 2:(NVDat.NetInfo.TimeFrame+1)
            tempTable(:,2) = tempTable(:,2) + (NVDat.SimResults.Network(t).Nodes.State == "sleeping");
            tempTable(:,3) = tempTable(:,3) + (NVDat.SimResults.Network(t).Nodes.State == "sensing");
            tempTable(:,4) = tempTable(:,4) + (NVDat.SimResults.Network(t).Nodes.State == "Tx");
            tempTable(:,5) = tempTable(:,5) + (NVDat.SimResults.Network(t).Nodes.State == "Rx");
        end
        
        tempTable = sortrows(tempTable, 'ascend');
        tempTable = tempTable(2:end,:);
        
        x = 1:(NVDat.NetInfo.NumberOfNodes-1);
        
        % X-Axis Ticks
        LastTransition = 1;
        TransitionCounter = 1;
        ticks = [];
        tickLabels = {};
        for s = 2:(NVDat.NetInfo.NumberOfNodes-1)
            if (tempTable(s, 1) ~= tempTable(s-1, 1))
                x(s:end) = x(s:end) + 1;
                
                ticks(TransitionCounter) = (LastTransition + x(s-1))/2;
                tickLabels{TransitionCounter} = sprintf('Distance %d', tempTable(s-1, 1));
                TransitionCounter = TransitionCounter + 1;
                LastTransition = x(s);
            end
        end
        ticks(TransitionCounter) = (LastTransition + x(end))/2;
        tickLabels{TransitionCounter} = sprintf('Distance %d', tempTable(end, 1));
        
        % Plot
        b = bar(ax, ...
            x, ...
            tempTable(:, 2:end)/NVDat.NetInfo.TimeFrame, ...
            'stacked');
        
        b(1).FaceColor = [149, 155, 165]/255;
        b(2).FaceColor = [138, 216, 43]/255;
        b(3).FaceColor = [6, 47, 112]/255;
        b(4).FaceColor = [13, 168, 196]/255;
        
        
        legend('sleeping', 'sensing', 'Tx', 'Rx');
        
        ax.YLabel.String = 'Time Percentage';
        ax.YLim(2) = 1;
        
        ax.XLabel.String = 'Nodes (ordered by increasing distance)';
        ax.XTick = ticks;
        ax.XTickLabel = tickLabels;
        ax.XTickLabelRotation = 45;
        
        ax.Title.String = 'Percentage of Time that each Node spends on certain State';
        
        fig.Name = ['Node State Time Percentages', ...
                    ' | H ', num2str(H), ' | alpha ', num2str(A)];
        
    end
end
