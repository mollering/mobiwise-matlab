clearvars 

fig = figure;
ax = axes(fig);
hold(ax, 'on');

chosenHarvesting = 2;
chosenAlpha = 5;

chosenOption = 5;


for H = chosenHarvesting
    for A = chosenAlpha
        
        fileName = sprintf('U4.mat', H, A);
        load(fileName);

%% Graphic Options
% 1: Range of Battery Values of All Nodes at a certain Time Period
%    Show Battery Min-Max of all the nodes during that Period
%    X-Axis: All Nodes - left: close to Sink; right - far from Sink
%            X-Axis Ticks separate the increasing distances
%    Y-Axis: Battery Range in Percentage
%    Chosen Time Period can be changed by 'chosenTslot' var
%           If chosenTslot is 1, then it shows the Battery Ranges during
%           all the Periods
chosenTslot = 1;

% 2: Range of Battery Values of Nodes Agregated by Distance at a certain Time Period
%    Show Avg Battery Min-Max of all the nodes with the same Distance to Sink
%    during that Period
%    X-Axis: Distance to Sink
%    Y-Axis: Battery Range in Percentage (Avg of all Nodes w/ certain
%            Distance)
%    Chosen Time Period can be changed by 'chosenTslot' var
%           If chosenTslot is 1, then it shows the Battery Ranges during
%           all the Periods
%chosenTslot = 23;

% 3: Battery Values of Nodes over Time
%    Show Battery Values of selected Nodes during all the simulation
%    X-Axis: Time
%    Y-Axis: Battery in Percentage
%    Chosen Nodes can be changed by 'chosenNode' var
%           If chosenNode only has 1 Node, show the Values for that Node
%           If has more than 1 Node, show the Avg Values for the selected Nodes
%           To Select all the nodes, use chosenNode = 1:NVDat.NetInfo.NumberOfNodes
chosenNode = 1;

% 4: Avg Battery Values of Nodes w/ same distance over Time
%    Show Avg Battery Values of all Nodes with selected Distance during all the simulation
%    X-Axis: Time
%    Y-Axis: Avg Battery in Percentage
%    Chosen Distance can be changed by 'chosenDistance' var
chosenDistance = 2;

% 5: Payloads On The Network over Time
%    Show all Payloads of the Selected Type during all the simulation
%    X-Axis: Time
%    Y-Axis: Number of Payloads
%    Chose the data to be shown
PayloadsAtSink = 'yes';
PayloadsOutsideSink = 'yes';
PayloadsCreated = 'no';
PayloadsOnEdges = 'no';


% 6: Payloads on Nodes over Time
%    Show Number of Payloads on selected Nodes during all the simulation
%    X-Axis: Time
%    Y-Axis: Number of Payloads
%    Chosen Nodes can be changed by 'chosenNode' var
%           If chosenNode only has 1 Node, show the Values for that Node
%           If has more than 1 Node, show the Avg Values for the selected Nodes
%           If chosen Node is Sink, it's the same graphic than 5 w/ PayloadsAtSink = 'yes';
%           To Select all the nodes, use chosenNode = 1:NVDat.NetInfo.NumberOfNodes
chosenNode = 1;

% 7: Payloads at Nodes w/ same distance over Time
%    Show (Avg/Total) Number of Payloads at all Nodes with selected Distance
%    during all the simulation
%    X-Axis: Time
%    Y-Axis: (Avg/Total) Number of Payloads
%    Chosen Distance can be changed by 'chosenDistance' var
%    If 'showAvg' is 'yes', then instead of showing the sum of all
%    Payloads at the Nodes of the specified Distance, it shows the Average
chosenDistance = 3;
showAvg = 'no';


switch chosenOption
    case 1
        YYOption = 'Battery';
        XXOption = 'Nodes';
        NodeAgregation = 'no';
        
        % XX Data
        % Order Nodes in Ascendent (close to far) Distance to Sink
        [~,sortedNodes] = sort(NVDat.NetInfo.Graph.Nodes.DistanceToSink);
        sortedNodes = sortedNodes(2:end);
        xxData = 1:(NVDat.NetInfo.NumberOfNodes-1);
        
        % YY Data
        thisPeriod = NVDat.SimResults.Network(chosenTslot).TimePeriod;
        yyData = zeros(1, (NVDat.NetInfo.NumberOfNodes-1));
        yyMax = yyData; yyMin = yyData;
        yyDataCounter = 1;
        
        % Iterate over the Nodes
        for n = xxData
            nodeID = sortedNodes(n);
            batteryData = zeros(1, NVDat.NetInfo.Alpha);
            xxDataCounter = 1;
            % Iterate over timeslots
            for t = 1:(NVDat.NetInfo.TimeFrame+1)
                % Select only the timeslots that correspond to this Period
                % If thisPeriod is 0, then select all the timeslots
                if (thisPeriod == 0 || ...
                        NVDat.SimResults.Network(t).TimePeriod == thisPeriod)
                    % Save the node battery data to temp array
                    batteryData(xxDataCounter) = ...
                        NVDat.SimResults.Network(t).Nodes.BatteryEnd(nodeID);
                    xxDataCounter = xxDataCounter + 1;
                end
            end
            yyData(yyDataCounter) = mean(batteryData);
            yyMax(yyDataCounter) = max(batteryData) - yyData(yyDataCounter);
            yyMin(yyDataCounter) = yyData(yyDataCounter) - min(batteryData);
            yyDataCounter = yyDataCounter + 1;
        end
        
        % UI Settings and Plots
        if (thisPeriod ~= 0)
            ax.Title.String = ...
                sprintf('Min-Max Battery at Period %d - all Nodes', thisPeriod);
        else
            ax.Title.String = 'Min-Max Battery at all Periods - all Nodes';
        end
        
        ax.XLim = [0 NVDat.NetInfo.NumberOfNodes];
        ax.XLabel.String = 'Nodes (ordered by increasing distance)';
        
        maxDistance = max(NVDat.NetInfo.Graph.Nodes.DistanceToSink);
        [countAtEachDistance,~] = ...
            histcounts(NVDat.NetInfo.Graph.Nodes.DistanceToSink(...
            ~NVDat.NetInfo.Graph.Nodes.IsSink), ...
            maxDistance);
        ax.XTick = cumsum(countAtEachDistance(1:(end-1)))+0.5;
        ax.XTickLabel = {};
        ax.TickLength(1) = 0.02;
        ax.TickDir  = 'both';
        ax.XGrid = 'on';
        
        ax.YLabel.String = 'Battery (%)';
        ax.YLim = [0 inf];
        ax.YGrid = 'on';
        ax.YMinorGrid = 'on';
        
        
        errorbar(ax, ...
            xxData, ...
            yyData, ...
            yyMin, ...
            yyMax, ...
            'Marker', 'none', ...
            'LineStyle', 'none', ...
            'LineWidth', 0.5, ...
            'DisplayName', ...
            ['H ', num2str(H), ' \alpha ', num2str(A)]);
        
        legend('Location', 'best');
        
        temp = line(ax, ax.XLim, [100 100], ...
            'Color','red', ...
            'LineStyle','--');
        temp.Annotation.LegendInformation.IconDisplayStyle = 'off';
             
    case 2
        YYOption = 'Battery';
        XXOption = 'Nodes';
        NodeAgregation = 'yes';
        
        % XX Data
        maxDistance = max(NVDat.NetInfo.Graph.Nodes.DistanceToSink);
        xxData = 1:maxDistance;
        
        % YY Data
        thisPeriod = NVDat.SimResults.Network(chosenTslot).TimePeriod;
        yyData = zeros(1, maxDistance);
        yyMax = yyData; yyMin = yyData;
        yyDataCounter = 1;
        
        % Iterate over the Nodes
        for n = xxData
            nodeIDs = (NVDat.NetInfo.Graph.Nodes.DistanceToSink == n);
            batteryData = zeros(1, NVDat.NetInfo.Alpha);
            xxDataCounter = 1;
            % Iterate over timeslots
            for t = 1:(NVDat.NetInfo.TimeFrame+1)
                % Select only the timeslots that correspond to this Period
                % If thisPeriod is 0, then select all the timeslots
                if (thisPeriod == 0 || ...
                        NVDat.SimResults.Network(t).TimePeriod == thisPeriod)
                    % Save the node battery data to temp array
                    batteryData(xxDataCounter) = ...
                        mean(NVDat.SimResults.Network(t).Nodes.BatteryEnd(nodeIDs));
                    xxDataCounter = xxDataCounter + 1;
                end
            end
            yyData(yyDataCounter) = mean(batteryData);
            yyMax(yyDataCounter) = max(batteryData) - yyData(yyDataCounter);
            yyMin(yyDataCounter) = yyData(yyDataCounter) - min(batteryData);
            yyDataCounter = yyDataCounter + 1;
        end
        
        % UI Settings and Plots
        if (thisPeriod ~= 0)
            ax.Title.String = ...
                sprintf('Min-Max Battery at Period %d - Nodes Agregated by Distance to Sink', ...
                thisPeriod);
        else
            ax.Title.String = ...
                'Min-Max Battery at all Periods - Nodes Agregated by Distance to Sink';
        end
        
        ax.XLim = [0 maxDistance+1];
        ax.XLabel.String = 'Distance to Sink';
        ax.XTick = xxData;
        ax.XGrid = 'on';
        
        ax.YLabel.String = 'Battery (%)';
        ax.YLim = [0 inf];
        ax.YGrid = 'on';
        ax.YMinorGrid = 'on';
        
        errorbar(ax, ...
            xxData, ...
            yyData, ...
            yyMin, ...
            yyMax, ...
            'Marker', 'none', ...
            'LineStyle', 'none', ...
            'LineWidth', 0.5, ...
            'DisplayName', ...
            ['H ', num2str(H), ' \alpha ', num2str(A)]);
        
        legend('Location', 'best');
        
        temp = line(ax, ax.XLim, [100 100], ...
            'Color','red', ...
            'LineStyle','--');
        temp.Annotation.LegendInformation.IconDisplayStyle = 'off';
        
    case 3
        YYOption = 'Battery';
        XXOption = 'Timeslot';
        NodeAgregation = 'no';
        
        % XX Data
        xxData = 1:(NVDat.NetInfo.TimeFrame+1);
        chosenNode(chosenNode == NVDat.NetInfo.SinkNode) = [];
        
        % YY Data
        yyData = zeros(1, (NVDat.NetInfo.TimeFrame+1));
        
        % Iterate over the timeslots
        for t = xxData
            yyData(t) = ...
                mean(NVDat.SimResults.Network(t).Nodes.BatteryEnd(chosenNode));
        end
        
        % UI Settings and Plots
        if (numel(chosenNode) == 1)
            ax.Title.String = ...
                sprintf('Battery of Node %d over time', chosenNode);
        elseif (numel(chosenNode) == NVDat.NetInfo.NumberOfNodes)
            ax.Title.String = 'Avg Battery of All Nodes over time';
        else
            ax.Title.String = 'Avg Battery of Selected Nodes over time';
        end
        
        ax.XLim = [0 (NVDat.NetInfo.TimeFrame+1)];
        ax.XLabel.String = 'Time';
        ax.XGrid = 'on';
        ax.XMinorGrid = 'on';
        
        ax.YLabel.String = 'Battery (%)';
        ax.YLim = [0 inf];
        ax.YGrid = 'on';
        ax.YMinorGrid = 'on';
        
        plot(ax, ...
            xxData-1, ...
            yyData, ...
            'LineWidth', 2, ...
            'DisplayName', ['H ', num2str(H), ' \alpha ', num2str(A)]);
        
        legend('Location', 'best');
        
        temp = line(ax, ax.XLim, [100 100], 'Color','red','LineStyle','--');
        temp.Annotation.LegendInformation.IconDisplayStyle = 'off';
        
    case 4
        YYOption = 'Battery';
        XXOption = 'Timeslot';
        NodeAgregation = 'yes';
        
        
        % XX Data
        nodeIDs = ...
            (NVDat.NetInfo.Graph.Nodes.DistanceToSink == chosenDistance);
        xxData = 1:(NVDat.NetInfo.TimeFrame+1);
        
        % YY Data
        yyData = zeros(1, (NVDat.NetInfo.TimeFrame+1));
        
        % Iterate over the timeslots
        for t = xxData
            yyData(t) = ...
                mean(NVDat.SimResults.Network(t).Nodes.BatteryEnd(nodeIDs));
        end
        
        % UI Settings and Plots
        ax.Title.String = ...
            sprintf('Avg Battery of Nodes with distance %d over time', ...
            chosenDistance);
        
        ax.XLim = [0 (NVDat.NetInfo.TimeFrame+1)];
        ax.XLabel.String = 'Time';
        ax.XGrid = 'on';
        ax.XMinorGrid = 'on';
        
        ax.YLabel.String = 'Battery (%)';
        ax.YLim = [0 inf];
        ax.YGrid = 'on';
        ax.YMinorGrid = 'on';
        
        plot(ax, ...
            xxData, ...
            yyData, ...
            'LineWidth', 2, ...
            'DisplayName', ['H ', num2str(H), ' \alpha ', num2str(A)]);
        
        legend('Location', 'best');
        
        temp = line(ax, ax.XLim, [100 100], ...
            'Color','red', ...
            'LineStyle','--');
        temp.Annotation.LegendInformation.IconDisplayStyle = 'off';
        
    case 5
        YYOption = 'Payloads On Network';
        XXOption = 'Time';
        
        % XX Data
        xxData = 0:(NVDat.NetInfo.TimeFrame);
        
        % YY Data
        yyData1 = zeros(1, (NVDat.NetInfo.TimeFrame+1));
        yyData2 = yyData1;
        yyData3 = yyData1;
        yyData4 = yyData1;
        
        % Iterate over the timeslots
        for t = 2:(NVDat.NetInfo.TimeFrame+1)
            yyData1(t) = ...
                NVDat.SimResults.Network(t).Totals.NumberOfPayloadsAtSink;
            yyData2(t) = ...
                NVDat.SimResults.Network(t).Totals.NumberOfPayloadsOutsideSink;
            yyData3(t) = ...
                NVDat.SimResults.Network(t).Totals.NumberOfPayloadsCreated;
            yyData4(t) = ...
                NVDat.SimResults.Network(t).Totals.NumberOfPayloadsOnEdges;
        end
        
        % UI Settings and Plots
        ax.Title.String = 'Payloads on the Network over time';
        
        ax.XLabel.String = 'Time';
        ax.XTick = (0:NVDat.NetInfo.NumberOfPeriods)*NVDat.NetInfo.Alpha;
        ax.XTickLabelRotation = 45;
        ax.XGrid = 'on';
        
        ax.YLabel.String = 'Number of Payloads';
        ax.YGrid = 'on';
        ax.YMinorGrid = 'on';
        
        if (strcmp(PayloadsAtSink, 'yes'))
            plot(ax, ...
                xxData, ...
                yyData1, ...
                'LineWidth', 2, ...
                'LineStyle', '-', ...
                'DisplayName', ...
                ['H ', num2str(H), ' \alpha ', num2str(A), ' Inside Sink']);
        end
        
        if (strcmp(PayloadsOutsideSink, 'yes'))
            plot(ax, ...
                xxData, ...
                yyData2, ...
                'LineWidth', 2, ...
                'LineStyle', '--', ...
                'DisplayName', ...
                ['H ', num2str(H), ' \alpha ', num2str(A), ' Outside Sink']);
        end
        
        if(strcmp(PayloadsCreated, 'yes'))
            plot(ax, ...
                xxData, ...
                yyData3, ...
                'LineWidth', 2, ...
                'LineStyle', ':', ...
                'DisplayName', ...
                ['H ', num2str(H), ' \alpha ', num2str(A), ' Created']);
        end
        
        if (strcmp(PayloadsOnEdges, 'yes'))
            plot(ax, ...
                xxData, ...
                yyData4, ...
                'LineWidth', 2, ...
                'LineStyle', '-.', ...
                'DisplayName', ...
                ['H ', num2str(H), ' \alpha ', num2str(A), ' Travelling On Edges']);
        end
        
       legend('Location', 'best');
    
    case 6
        YYOption = 'Payloads';
        XXOption = 'Timeslot';
        NodeAgregation = 'no';
        
        % XX Data
        xxData = 0:(NVDat.NetInfo.TimeFrame);
        
        % YY Data
        yyData = zeros(1, (NVDat.NetInfo.TimeFrame+1));
        
        % Iterate over the timeslots
        for t =  2:(NVDat.NetInfo.TimeFrame+1)
            yyData(t) = ...
                mean(NVDat.SimResults.Network(t).Nodes.PayloadsEnd(chosenNode));
        end
        
        % UI Settings and Plots
        if (numel(chosenNode) == 1)
            ax.Title.String = ...
                sprintf('Payloads at Node %d (distance %d) over time', ...
                chosenNode, ...
                NVDat.NetInfo.Graph.Nodes.DistanceToSink(chosenNode));
        elseif (numel(chosenNode) == NVDat.NetInfo.NumberOfNodes)
            ax.Title.String = 'Avg Payloads at All Nodes over time';
        else
            ax.Title.String = 'Avg Payloads at Selected Nodes over time';
        end
        
        ax.XLim = [0 (NVDat.NetInfo.TimeFrame+1)];
        ax.XTick = (0:NVDat.NetInfo.NumberOfPeriods)*NVDat.NetInfo.Alpha;
        ax.XTickLabelRotation = 45;
        ax.XLabel.String = 'Time';
        ax.XGrid = 'on';
        ax.XMinorGrid = 'on';
        
        ax.YLabel.String = 'Number of Payloads';
        ax.YLim = [0 inf];
        ax.YGrid = 'on';
        ax.YMinorGrid = 'on';
        
        plot(ax, ...
            xxData, ...
            yyData, ...
            'LineWidth', 2, ...
            'DisplayName',  ['H ', num2str(H), ' \alpha ', num2str(A)]);
        
        legend('Location', 'best');
        
    case 7
        YYOption = 'Payloads';
        XXOption = 'Timeslot';
        NodeAgregation = 'yes';
        
        
        % XX Data
        nodeIDs = ...
            (NVDat.NetInfo.Graph.Nodes.DistanceToSink == chosenDistance);
        xxData = 0:(NVDat.NetInfo.TimeFrame);
        
        % YY Data
        yyData = zeros(1, (NVDat.NetInfo.TimeFrame+1));
        
        % Iterate over the timeslots
        for t = 2:(NVDat.NetInfo.TimeFrame+1)
            if (strcmp(showAvg, 'yes'))
                yyData(t) = ...
                    mean(NVDat.SimResults.Network(t).Nodes.PayloadsEnd(nodeIDs));
            else
                yyData(t) = ...
                    sum(NVDat.SimResults.Network(t).Nodes.PayloadsEnd(nodeIDs));
            end
        end
        
        % UI Settings and Plots
        if (strcmp(showAvg, 'yes'))
            ax.Title.String = ...
                sprintf('Avg Payloads at Nodes with distance %d over time', ...
                chosenDistance);
        else
            ax.Title.String = ...
                sprintf('Total Payloads at Nodes with distance %d over time', ...
                chosenDistance);
        end
        
        ax.XLim = [0 (NVDat.NetInfo.TimeFrame+1)];
        ax.XTick = (0:NVDat.NetInfo.NumberOfPeriods)*NVDat.NetInfo.Alpha;
        ax.XTickLabelRotation = 45;
        ax.XLabel.String = 'Time';
        ax.XGrid = 'on';
        ax.XMinorGrid = 'on';
        
        ax.YLabel.String = 'Number of Payloads';
        ax.YLim = [0 inf];
        ax.YGrid = 'on';
        ax.YMinorGrid = 'on';
        
        plot(ax, ...
            xxData, ...
            yyData, ...
            'LineWidth', 2, ...
            'DisplayName', ['H ', num2str(H), ' \alpha ', num2str(A)]);
        
        legend('Location', 'best');

end




    end
end
