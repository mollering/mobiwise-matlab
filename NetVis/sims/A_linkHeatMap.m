clearvars

%% Data Extraction
uiopen('load');

% Use graph as a digraph
Edges = NVDat.NetInfo.Graph.Edges.EndNodes;
Edges = [Edges; fliplr(Edges)];
% Remove edges going out from Sink Node
Edges(Edges(:,1) == NVDat.NetInfo.SinkNode, :) = [];

% Create Table
Edges = table(Edges(:,1), Edges(:,2), 'VariableNames', {'From', 'To'});
Graph = digraph(Edges.From, Edges.To);
Edges.From = Graph.Edges.EndNodes(:,1);
Edges.To = Graph.Edges.EndNodes(:,2);
Edges.Packets  = zeros(height(Edges), 1);
Edges.Payloads = zeros(height(Edges), 1);
Edges.Distance = zeros(height(Edges), 1);

% Get total number of Packets and Payloads
for t = 2:(NVDat.NetInfo.TimeFrame+1)
    for e = 1:NVDat.SimResults.Network(t).Totals.NumberOfActiveEdges
        
        idx = (Edges.From == NVDat.SimResults.Network(t).ActiveEdges.NodeFrom(e) & ...
            Edges.To == NVDat.SimResults.Network(t).ActiveEdges.NodeTo(e));
        
        Edges.Packets(idx)  = Edges.Packets(idx)  + NVDat.SimResults.Network(t).ActiveEdges.NumberOfHeaders(e);
        Edges.Payloads(idx) = Edges.Payloads(idx) + NVDat.SimResults.Network(t).ActiveEdges.NumberOfPayloads(e);
        Edges.Distance(idx) = max([NVDat.NetInfo.Graph.Nodes.DistanceToSink(Edges.From(idx)), NVDat.NetInfo.Graph.Nodes.DistanceToSink(Edges.To(idx))]);

    end
end

TotalPayloadsSent = max(Edges.Payloads);
Edges.PayloadPct = Edges.Payloads/TotalPayloadsSent;
TotalPayloadsAtSink = NVDat.SimResults.Network(NVDat.NetInfo.TimeFrame+1).Totals.NumberOfPayloadsAtSink;
TotalPayloadsOutSink = NVDat.SimResults.Network(NVDat.NetInfo.TimeFrame+1).Totals.NumberOfPayloadsOutsideSink;


%% Graphical Representation
fig = figure;
fig.NumberTitle = 'off';
fig.Name = 'Edges Activity';
fig.Color = [1,1,1];
fig.Position = [1921,41,1920,963];

ax = axes(fig);
hold(ax, 'on');
ax.Units = 'normalized';


p = plot(ax, ...
    Graph, ...
    'XData',         NVDat.NetInfo.Graph.Nodes.XYCoords(:,1), ...
    'YData',         NVDat.NetInfo.Graph.Nodes.XYCoords(:,2), ...
    'Marker',        'o', ...
    'MarkerSize',    11, ...
    'LineStyle',     '-', ...%'NodeCData', ones(1, NVDat.NetInfo.NumberOfNodes), ...
    'EdgeCData',     ones(1, height(Edges)), ...
    'NodeColor',     [0.15,0.15,0.15], ...
    'EdgeColor',     'flat', ...
    'NodeLabel',     [], ...
    'LineWidth',     5, ...
    'ArrowSize',     15, ...
    'ArrowPosition', 0.75, ...
    'EdgeFontSize',  9, ...
    'EdgeFontWeight', 'bold' ...
    );

% Sink Node
scatter(ax, ...
    NVDat.NetInfo.Graph.Nodes.XYCoords(NVDat.NetInfo.SinkNode,1), ...
    NVDat.NetInfo.Graph.Nodes.XYCoords(NVDat.NetInfo.SinkNode,2), ...
    280, ...
    'k', 'filled');

% Axis Settings
ax.XAxis.Visible = 'off';
ax.YAxis.Visible = 'off';
axis vis3d

% Edge Labels
labeledge(p, ...
    Edges.From, ...
    Edges.To, ...
    arrayfun(@(x,y) sprintf('         [%d] %d', x,y), Edges.Packets, Edges.Payloads, 'UniformOutput', false));
p.EdgeLabel(Edges.Payloads == 0) = {''};



NodeIDLabels = text(...
    ax, ...
    NVDat.NetInfo.Graph.Nodes.XYCoords(:,1), ...
    NVDat.NetInfo.Graph.Nodes.XYCoords(:,2), ...
    '', ...
    'FontUnits',           'points', ...
    'FontSize',            9, ...
    'FontWeight',          'bold', ...
    'Color',               'white', ...
    'HorizontalAlignment', 'center', ...
    'VerticalAlignment',   'middle', ...
    'Visible', 'on' ...
    );
for node = 1:NVDat.NetInfo.NumberOfNodes
    if (NVDat.SimResults.Network(NVDat.NetInfo.TimeFrame+1).Nodes.PayloadsEnd(node) > 0)
        NodeIDLabels(node).String = ...
            sprintf('%d', NVDat.SimResults.Network(NVDat.NetInfo.TimeFrame+1).Nodes.PayloadsEnd(node));
    end
end
                
% p.NodeLabel(1:25) = cellstr('');
% for node = 1:NVDat.NetInfo.NumberOfNodes
%     if (NVDat.SimResults.Network(NVDat.NetInfo.TimeFrame+1).Nodes.PayloadsEnd(node) > 0)
%         p.NodeLabel{node} = ...
%             sprintf(' %d', NVDat.SimResults.Network(NVDat.NetInfo.TimeFrame+1).Nodes.PayloadsEnd(node));
%     end
% end
% p.NodeLabel{NVDat.NetInfo.SinkNode} = '';

% Color Data
p.EdgeCData = Edges.Payloads;
ax.CLim = ...
    [min(Edges.Payloads) ...
    max(Edges.Payloads)];
c = colorbar(ax);
c.Label.String = 'Activity ([# Packets] # Payloads)';
c.Label.FontWeight = 'bold';
load('Z_EdgesColorMap');
colormap(ax, EdgesColorMap);

% Title
ax.Title.FontWeight = 'normal'; 
ax.Title.String = {['\bfLink activity heat map']; ...
    ['\rm', num2str(TotalPayloadsAtSink), ' payloads at sink | ', num2str(TotalPayloadsOutSink), ' outside of sink']};
ax.Title.Position = [2.987287916949843,5.23362461155005,0];

ax.Colorbar.Visible = 'off';
ax.Colorbar.Position = [0.705729166666667 0.186915887850467 0.0109375000000004 0.666795902542544];
