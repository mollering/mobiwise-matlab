

clearvars
close all

fig = figure;
fig.Position = [1921,41,1920,963];
fig.Color = [1,1,1];
ax = axes(fig);
hold(ax, 'on');


y = zeros(4,2);
tbl = extractAggregation('U7.mat'); % low 
for d = 1:4
    y(d,1) = mean(tbl.completion(tbl.distance == d));
end

tbl = extractAggregation('U6.mat'); % medium 
for d = 1:4
    y(d,2) = mean(tbl.completion(tbl.distance == d));
end

tbl = extractAggregation('U10.mat'); % high 
for d = 1:4
    y(d,3) = mean(tbl.completion(tbl.distance == d));
end


h = bar(ax, y);

h(1).FaceColor = [0.80,0.80,0.80];
h(2).FaceColor = [0.50,0.50,0.50];
h(3).FaceColor = [0.15,0.15,0.15];

axis square
l = cell(1,2);
l{1} = 'low harvesting';
l{2} = 'medium harvesting';
l{3} = 'high harvesting';
leg = legend(h, l);
leg.FontSize = 12;
leg.Box = 'off';
leg.FontAngle = 'italic';

ax.Title.String = 'Data aggregation comparison';
ax.Title.FontSize = 13.5;
ax.Title.FontName = 'DejaVu Sans Condensed';

ax.XLabel.String = 'Distance to \bfsink node';
ax.YLabel.String = 'Average data aggregation in performed transmissions \it(in percentage of full data packet)';
ax.YGrid = 'on';
ax.YMinorGrid = 'on';
ax.YLabel.FontAngle = 'italic';
ax.YLabel.FontSize = 14;
ax.YLabel.FontName = 'Calibri';





ax.XTick = 1:4;
for t = 1:4
    ax.XTickLabel(t) = cellstr(sprintf('%d-hop', t));
end
ax.XLabel.FontAngle = 'italic';
ax.XLabel.FontSize = 14;
ax.XLabel.FontName = 'Calibri';
ax.YLim = [0, 1];


function tbl = extractAggregation(file)
load(file);
% Create Table
tbl = array2table(zeros(0,5), ...
    'VariableNames', {'tslot', 'nodeID', 'distance', 'payloads', 'completion'});

warning('off', 'MATLAB:table:RowsAddedExistingVars');
 
aux = 1;
for t = 1:(NVDat.NetInfo.TimeFrame+1)
   for c = 1:height(NVDat.SimResults.Network(t).ActiveEdges)
       tbl.tslot(aux)      = NVDat.SimResults.Network(t).TimeSlot;
       tbl.nodeID(aux)     = NVDat.SimResults.Network(t).ActiveEdges.NodeFrom(c);
       tbl.distance(aux)   = NVDat.NetInfo.Graph.Nodes.DistanceToSink(tbl.nodeID(aux));
       tbl.payloads(aux)   = NVDat.SimResults.Network(t).ActiveEdges.NumberOfPayloads(c);
       tbl.completion(aux) = tbl.payloads(aux)/NVDat.NetInfo.MaxPayloadsPerMsg;
       aux = aux + 1;
   end
end

warning('on', 'MATLAB:table:RowsAddedExistingVars');


auxtbl = array2table(zeros(0,5), ...
    'VariableNames', {'tslot', 'nodeID', 'distance', 'payloads', 'completion'});

aux = 1;
for i = 1:height(tbl)
    while (tbl.payloads(i) > NVDat.NetInfo.MaxPayloadsPerMsg)
        auxtbl(aux,:) = tbl(i,:);
        auxtbl.payloads(aux)   = NVDat.NetInfo.MaxPayloadsPerMsg;
        auxtbl.completion(aux) = auxtbl.payloads(aux)/NVDat.NetInfo.MaxPayloadsPerMsg;
        tbl.payloads(i) = tbl.payloads(i) - NVDat.NetInfo.MaxPayloadsPerMsg;
        aux = aux + 1;
    end
    auxtbl(aux,:) = tbl(i,:);
    auxtbl.completion(aux) = auxtbl.payloads(aux)/NVDat.NetInfo.MaxPayloadsPerMsg;
    aux = aux + 1;
end

tbl = auxtbl;
end
