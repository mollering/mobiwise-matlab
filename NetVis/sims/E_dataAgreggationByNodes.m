clearvars

%% Data Extraction
uiopen('load');

% Create Table
tbl = array2table(zeros(0,5), ...
    'VariableNames', {'tslot', 'nodeID', 'distance', 'payloads', 'completion'});

warning('off', 'MATLAB:table:RowsAddedExistingVars');
 
aux = 1;
for t = 1:(NVDat.NetInfo.TimeFrame+1)
   for c = 1:height(NVDat.SimResults.Network(t).ActiveEdges)
       tbl.tslot(aux)      = NVDat.SimResults.Network(t).TimeSlot;
       tbl.nodeID(aux)     = NVDat.SimResults.Network(t).ActiveEdges.NodeFrom(c);
       tbl.distance(aux)   = NVDat.NetInfo.Graph.Nodes.DistanceToSink(tbl.nodeID(aux));
       tbl.payloads(aux)   = NVDat.SimResults.Network(t).ActiveEdges.NumberOfPayloads(c);
       tbl.completion(aux) = tbl.payloads(aux)/NVDat.NetInfo.MaxPayloadsPerMsg;
       aux = aux + 1;
   end
end

warning('on', 'MATLAB:table:RowsAddedExistingVars');

auxtbl = array2table(zeros(0,5), ...
    'VariableNames', {'tslot', 'nodeID', 'distance', 'payloads', 'completion'});

aux = 1;
for i = 1:height(tbl)
    while (tbl.payloads(i) > NVDat.NetInfo.MaxPayloadsPerMsg)
        auxtbl(aux,:) = tbl(i,:);
        auxtbl.payloads(aux)   = NVDat.NetInfo.MaxPayloadsPerMsg;
        auxtbl.completion(aux) = auxtbl.payloads(aux)/NVDat.NetInfo.MaxPayloadsPerMsg;
        tbl.payloads(i) = tbl.payloads(i) - NVDat.NetInfo.MaxPayloadsPerMsg;
        aux = aux + 1;
    end
    auxtbl(aux,:) = tbl(i,:);
    auxtbl.completion(aux) = auxtbl.payloads(aux)/NVDat.NetInfo.MaxPayloadsPerMsg;
    aux = aux + 1;
end

tbl = auxtbl;

%% Graphics

h = heatmap(tbl, 'tslot', 'nodeID', 'ColorVariable', 'completion');
h.Title = {['Average Aggregation of Packets Sent by Nodes and by Time Slot']; ...
    ['Square Network with ', num2str(NVDat.NetInfo.NumberOfNodes),' Nodes during ', num2str(NVDat.NetInfo.TimeFrame), ' TimeSlots']; ...
    ['Value of 1 means that was sent a Full Packet']};
h.XLabel = 'Time Slot';
h.YLabel = 'Distance to Sink';
h.MissingDataLabel = '0';
h.MissingDataColor = 'w';
h.GridVisible = 'off';
[sorted, sortedidx] = sort(NVDat.NetInfo.Graph.Nodes.DistanceToSink, 'descend');
h.YDisplayData = cellstr(num2str(sortedidx(1:end-1)));
h.YDisplayLabels = cellstr(num2str(sorted(1:end-1)));
h.XDisplayData = cellstr(num2str((1:NVDat.NetInfo.TimeFrame)'));

f = gcf;
f.Color = 'w';
f.Position = [1921,41,1920,963];
h.OuterPosition = [-0.128531586021506,-0.030732182787684,1.21706989247312,1.034599188374775];
h.Position = [0.0296875,0.083073727933541,0.922395833333333,0.843198338525441];
