clearvars

%% Data Extraction
uiopen('load');

% Create Table
tbl = array2table(zeros(0,5), ...
    'VariableNames', {'tslot', 'nodeID', 'distance', 'payloads', 'completion'});

warning('off', 'MATLAB:table:RowsAddedExistingVars');
 
aux = 1;
for t = 1:(NVDat.NetInfo.TimeFrame+1)
   for c = 1:height(NVDat.SimResults.Network(t).ActiveEdges)
       tbl.tslot(aux)      = NVDat.SimResults.Network(t).TimeSlot;
       tbl.nodeID(aux)     = NVDat.SimResults.Network(t).ActiveEdges.NodeFrom(c);
       tbl.distance(aux)   = NVDat.NetInfo.Graph.Nodes.DistanceToSink(tbl.nodeID(aux));
       tbl.payloads(aux)   = NVDat.SimResults.Network(t).ActiveEdges.NumberOfPayloads(c);
       tbl.completion(aux) = tbl.payloads(aux)/NVDat.NetInfo.MaxPayloadsPerMsg;
       aux = aux + 1;
   end
end

warning('on', 'MATLAB:table:RowsAddedExistingVars');


auxtbl = array2table(zeros(0,5), ...
    'VariableNames', {'tslot', 'nodeID', 'distance', 'payloads', 'completion'});

aux = 1;
for i = 1:height(tbl)
    while (tbl.payloads(i) > NVDat.NetInfo.MaxPayloadsPerMsg)
        auxtbl(aux,:) = tbl(i,:);
        auxtbl.payloads(aux)   = NVDat.NetInfo.MaxPayloadsPerMsg;
        auxtbl.completion(aux) = auxtbl.payloads(aux)/NVDat.NetInfo.MaxPayloadsPerMsg;
        tbl.payloads(i) = tbl.payloads(i) - NVDat.NetInfo.MaxPayloadsPerMsg;
        aux = aux + 1;
    end
    auxtbl(aux,:) = tbl(i,:);
    auxtbl.completion(aux) = auxtbl.payloads(aux)/NVDat.NetInfo.MaxPayloadsPerMsg;
    aux = aux + 1;
end

tbl = auxtbl;

%% Graphics

%yy = categorical(tbl.nodeID);

h = heatmap(tbl, 'tslot', 'distance', 'ColorVariable', 'completion');
h.Title = {['Average aggregation of data packets']};
h.XLabel = 'Time Slot';
h.YLabel = 'Distance to Sink Node';
h.FontName = 'DejaVu Sans Condensed';
h.FontSize = 11;
h.MissingDataLabel = '0';
h.MissingDataColor = 'w';
h.GridVisible = 'off';
h.YDisplayData = flip(h.YDisplayData);
h.XDisplayData = cellstr(num2str((1:NVDat.NetInfo.TimeFrame)'));
h.XDisplayLabels = cellstr(num2str((1:NVDat.NetInfo.TimeFrame)'));
h.XDisplayLabels(2:9) = cellstr(' ');
h.XDisplayLabels(11:19) = cellstr(' ');
h.XDisplayLabels(21:29) = cellstr(' ');
h.XDisplayLabels(31:39) = cellstr(' ');
h.XDisplayLabels(41:49) = cellstr(' ');
h.XDisplayLabels(51:59) = cellstr(' ');

f = gcf;
f.Color = 'w';
f.Position = [2434,56,984,931];
% h.OuterPosition = [-0.128531586021506,-0.030732182787684,1.21706989247312,1.034599188374775];
% h.Position = [0.0296875,0.083073727933541,0.922395833333333,0.843198338525441];
