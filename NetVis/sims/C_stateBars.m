clearvars

%% Data Extraction
uiopen('load');


tempTable = zeros(NVDat.NetInfo.NumberOfNodes, 5);
tempTable(:,1) = NVDat.NetInfo.Graph.Nodes.DistanceToSink;
for t = 2:(NVDat.NetInfo.TimeFrame+1)
    tempTable(:,2) = tempTable(:,2) + (NVDat.SimResults.Network(t).Nodes.State == "sleeping");
    tempTable(:,3) = tempTable(:,3) + (NVDat.SimResults.Network(t).Nodes.State == "sensing");
    tempTable(:,4) = tempTable(:,4) + (NVDat.SimResults.Network(t).Nodes.State == "Tx");
    tempTable(:,5) = tempTable(:,5) + (NVDat.SimResults.Network(t).Nodes.State == "Rx");
end

tempTable = sortrows(tempTable, 'ascend');
tempTable = tempTable(2:end,:);

x = 1:(NVDat.NetInfo.NumberOfNodes-1);


%% Graphical Options
fig = figure;
fig.NumberTitle = 'off';
fig.Name = 'Node States over time';
fig.Color = [1,1,1];
fig.Position = [1921,41,1920,963];

ax = axes(fig);
hold(ax, 'on');
ax.Units = 'normalized';

% X-Axis Ticks
LastTransition = 1;
TransitionCounter = 1;
ticks = [];
tickLabels = {};

for s = 2:(NVDat.NetInfo.NumberOfNodes-1)
    if (tempTable(s, 1) ~= tempTable(s-1, 1))
        x(s:end) = x(s:end) + 1;
        
        ticks(TransitionCounter) = (LastTransition + x(s-1))/2;
        
        tickLabels{TransitionCounter} = sprintf('%d-hop', tempTable(s-1, 1));
        TransitionCounter = TransitionCounter + 1;
        LastTransition = x(s);
    end
end
ticks(TransitionCounter) = (LastTransition + x(end))/2;
tickLabels{TransitionCounter} = sprintf('%d-hop', tempTable(end, 1));

% Plot
b = bar(ax, ...
    x, ...
    tempTable(:, 2:end)/NVDat.NetInfo.TimeFrame, ...
    'stacked');

b(1).FaceColor = [149, 155, 165]/255;
b(2).FaceColor = [138, 216, 43]/255;
b(3).FaceColor = [6, 47, 112]/255;
b(4).FaceColor = [13, 168, 196]/255;

% leg = legend('sleeping', 'sensing', 'Tx', 'Rx', 'Location', 'northeastoutside');
% leg.FontSize = 10;
% leg.FontAngle = 'italic';

ylabel('Time Percentage','FontSize',14, 'FontAngle', 'italic', ...
    'FontName','Calibri', 'Position',[-0.689596582775161,0.500000476837158,-0.999999999999986]);

ax.YLim(2) = 1;

xlabel('Nodes (ordered by increasing distance to the \bfsink node\rm)',...
    'FontSize',14,'FontAngle', 'italic', ...
    'FontName','Calibri', 'Position', [14.000013351440417,-0.023779192560544,-0.999999999999986]);

ax.XTick = ticks;
ax.XTickLabel = tickLabels;
ax.XTickLabelRotation = 0;
ax.TickLength = [0.005,0.02];
ax.YGrid = 'on';
ax.XLim = [0,28];

title({'Percentage of the overall time that each node spends on a certain operational state'},...
    'FontWeight','bold',...
    'FontSize',13.5,...
    'FontName','DejaVu Sans Condensed',...
    'Interpreter','none',...
     'Position', [13.714683527855335,1.00047770700637,1.4e-14]);


axis square

hold off;