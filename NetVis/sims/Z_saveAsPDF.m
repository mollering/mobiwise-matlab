fig = gcf;

set(fig,'Units','Inches');
pos = get(fig,'Position');
set(fig,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)]);


% Name of file
fileName = sprintf('Print_N%d_T%d', NVDat.NetInfo.NumberOfNodes, NVDat.NetInfo.TimeFrame);
print(fig, fileName, '-dpdf', '-r0');