clearvars

%% Data Extraction
uiopen('load');

% Create Table
tbl = table('Size', [0,4], 'VariableTypes', {'double', 'double', 'double', 'double'}, ...
    'VariableNames', {'tslot', 'nodeID', 'distance', 'state'});

warning('off', 'MATLAB:table:RowsAddedExistingVars');
 
aux = 1;
for t = 2:(NVDat.NetInfo.TimeFrame+1)
   for n = 1:height(NVDat.SimResults.Network(t).Nodes)
       if n ~= NVDat.NetInfo.SinkNode
           tbl.tslot(aux)      = NVDat.SimResults.Network(t).TimeSlot;
           tbl.nodeID(aux)     = n;
           tbl.distance(aux)   = NVDat.NetInfo.Graph.Nodes.DistanceToSink(tbl.nodeID(aux));
           if NVDat.SimResults.Network(t).Nodes.State(n) == 'sensing'
               eventFound = 0;
                for e = 1:height(NVDat.SimResults.Events)
                    if NVDat.SimResults.Events.DetectedFirstAtTimeSlot(e) == t
                        eventFound = 1;
                        for c = 1:length(NVDat.SimResults.Events.NearbyNodes{e})
                            if NVDat.SimResults.Events.NearbyNodes{e}(c) == n
                                tbl.state(aux)  = 5;
                                break;
                            else
                                tbl.state(aux)  = 1;
                            end
                        end
                    end
                end
                if eventFound == 0
                    tbl.state(aux)  = 1;
                end
           elseif NVDat.SimResults.Network(t).Nodes.State(n) == 'Rx'
                tbl.state(aux)  = 2;
            elseif NVDat.SimResults.Network(t).Nodes.State(n) == 'Tx'
            tbl.state(aux)      = 3;
            elseif NVDat.SimResults.Network(t).Nodes.State(n) == 'sleeping'
            tbl.state(aux)      = 4;
           end
           aux = aux + 1;
       end
   end
end

warning('on', 'MATLAB:table:RowsAddedExistingVars');

%% Graphics

f = figure;
h = heatmap(tbl, 'tslot', 'nodeID', 'ColorVariable', 'state');
h.Title = {['Node States Allocation through time']; ...
    ['Square Network with ', num2str(NVDat.NetInfo.NumberOfNodes),' Nodes during ', num2str(NVDat.NetInfo.TimeFrame), ' TimeSlots']; ...
    ['Green = Sensing (if dark = event captured) | Blue = Tx(dark)/Rx(light) | Grey = sleeping']};
h.XLabel = 'Time Slot';
h.YLabel = 'Distance to Sink';
h.MissingDataLabel = '0';
h.MissingDataColor = 'w';
h.GridVisible = 'on';
h.ColorbarVisible = 'off';
h.CellLabelColor = 'none';
[sorted, sortedidx] = sort(NVDat.NetInfo.Graph.Nodes.DistanceToSink, 'descend');
h.YDisplayData = cellstr(num2str(sortedidx(1:end-1)));
h.YDisplayLabels = cellstr(num2str(sorted(1:end-1)));
h.XDisplayData = cellstr(num2str((1:NVDat.NetInfo.TimeFrame)'));
h.Colormap = [[138, 216, 43]/255; [13, 168, 196]/255; [6, 47, 112]/255; [149, 155, 165]/255; [10, 109, 30]/255];

f.Color = 'w';
f.Position = [1921,41,1920,963];
h.OuterPosition = [-0.128531586021506,-0.030732182787684,1.21706989247312,1.034599188374775];
h.Position = [0.0296875,0.083073727933541,0.922395833333333,0.843198338525441];