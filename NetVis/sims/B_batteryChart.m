clearvars

%% Data Extraction
uiopen('load');

% 3: Battery Values of Nodes over Time
%    Show Battery Values of selected Nodes during all the simulation
%    X-Axis: Time
%    Y-Axis: Battery in Percentage
%    Chosen Nodes can be changed by 'chosenNode' var
%           If chosenNode only has 1 Node, show the Values for that Node
%           If has more than 1 Node, show the Avg Values for the selected Nodes
%           To Select all the nodes, use chosenNode = 1:NVDat.NetInfo.NumberOfNodes

% XX Data
XXOption = 'Timeslot';
chosenNode = 1:NVDat.NetInfo.NumberOfNodes;
xxData = 1:(NVDat.NetInfo.TimeFrame+1);
chosenNode(chosenNode == NVDat.NetInfo.SinkNode) = [];

% YY Data
YYOption = 'Battery';
yyData = zeros(1, (NVDat.NetInfo.TimeFrame+1));

%% UI Settings and Plots

fig = figure;
fig.NumberTitle = 'off';
fig.Name = 'Battery Over Time';
fig.Color = [1,1,1];
fig.Position = [1921,41,1920,963];

ax = axes(fig);
yyaxis(ax, 'left');
hold(ax, 'on');

plot(-1, -1, 'o', 'color', [1, 1, 1], 'visible', 'on', 'DisplayName', '\bf\fontsize{12}Left Axis');

for d = 1:max(NVDat.NetInfo.Graph.Nodes.DistanceToSink)
    
    %XX Data
    nodeIDs = ...
        (NVDat.NetInfo.Graph.Nodes.DistanceToSink == d);
    xxData = 1:(NVDat.NetInfo.TimeFrame+1);

    %YY Data
    yyData = zeros(1, (NVDat.NetInfo.TimeFrame+1));

    %Iterate over the timeslots
    for t = xxData
        yyData(t) = ...
            mean(NVDat.SimResults.Network(t).Nodes.BatteryEnd(nodeIDs));
    end
    
    p = plot(ax, ...
    xxData-1, ...
    yyData, ...
    'LineWidth', 1, ...
    'DisplayName', sprintf('%d-hop average', d));
    
    switch d
        case 1
            p.Color  = [0.47,0.67,0.19];
            p.Marker = '+';
        case 2
            p.Color = [0.20,0.30,0.05];
            p.Marker = 'o';
            p.MarkerSize = 4;
            p.MarkerFaceColor = p.Color;
        case 3
            p.Color = [0.07,0.62,1.00];
            p.Marker = 'diamond';
            p.MarkerSize = 4;
            p.MarkerFaceColor = p.Color;
        case 4
            p.Color = [0.00,0.45,0.74];
            p.Marker = 'square';
            p.MarkerSize = 5;
            p.MarkerFaceColor = p.Color;
    end
    
end

% Iterate over the timeslots
for t = xxData
    yyData(t) = ...
        mean(NVDat.SimResults.Network(t).Nodes.BatteryEnd(chosenNode));
end

plot(ax, ...
    xxData-1, ...
    yyData, ...
    'LineWidth', 1.5, ...
    'Color', 'b', ...
    'Marker', 'none', ...
    'DisplayName', 'network average');


plot(-1, -1, 'o', 'color', [1, 1, 1], 'visible', 'on', 'DisplayName', '\bf\fontsize{12}Right Axis');


% Stuff
title({'Battery of nodes over time'},...
    'FontWeight','bold',...
    'FontSize',13.5,...
    'FontName','DejaVu Sans Condensed',...
    'Interpreter','none');

ylabel('Battery (%)','FontSize',14,  'FontWeight','bold', 'FontAngle', 'italic', ...
    'FontName','Calibri', 'Position',[-1.454501176736066,50.000019073486342,-1]);

set(ax,'YColor',[0.02,0.28,0.44]);
ax.YLim = [30, 70];

nLeftTicks = length(ax.YTick);


%% Right
yyaxis right

avgHarv = mean([NVDat.SimResults.Network(2:(NVDat.NetInfo.TimeFrame+1)).Harvesting]);

plot(xxData-1, ...
    [NVDat.SimResults.Network.Harvesting], ...
    'LineWidth', 2, ...
    'Color', [0.15,0.15,0.15], ...
    'LineStyle', ':', ...
    'Marker', 'none', ...
    'MarkerSize', 10, ...
    'DisplayName', 'harvesting');

plot(xxData-1, ...
    avgHarv*ones((NVDat.NetInfo.TimeFrame+1), 1), ...
    'LineWidth', 1.5, ...
    'Color', [0.15,0.15,0.15], ...
    'LineStyle', ':', ...
    'Marker', '+', ...
    'MarkerSize', 8, ...
    'DisplayName', 'harvesting overall average');

axis([0 (NVDat.NetInfo.TimeFrame+1) 0 12]);
ylabel('Harvesting (%)','FontSize',14,  'FontWeight','bold', 'FontAngle', 'italic', ...
    'FontName','Calibri', 'Position',[62.42462838989409,6.000005722045898,-1]);


ax2Ticks = ax.YLim(2)/(nLeftTicks-1);
ax.YTick = 0:ax2Ticks:ax.YLim(2);

ax.YColor = [0.40,0.40,0.40];
xlabel('Time', 'FontWeight','bold');
axis square

set(ax,'FontAngle','italic','LineStyleOrderIndex',6,'XGrid','on',...
    'XMinorGrid','on','YGrid','on','YMinorGrid','on');
% temp = line(ax, ax.XLim, [100 100], 'Color','red','LineStyle','--');
% temp.Annotation.LegendInformation.IconDisplayStyle = 'off';
% temp = line(ax, ax.XLim, [10 10], 'Color','red','LineStyle','--');
% temp.Annotation.LegendInformation.IconDisplayStyle = 'off';



leg = legend('Location', 'northwest');
leg.FontSize = 12;
leg.Box = 'off';
leg.FontAngle = 'italic';
%leg.Position = [0.805625000614673,0.356488096657256,0.073124998770654,0.102380949542636];

% ax.YTickLabel{ax.YTick == 10} =  'min';
% ax.YTickLabel{ax.YTick == 100} = 'max';
