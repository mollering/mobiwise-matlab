%% Plot data as is
figure
AllDataFig = plot(EnergyTraceTX5Bytes.Time, EnergyTraceTX5Bytes.Current);

%%
% ^ From figure we extract manually meaningful data frames
% and save them on PeakData
% ^ From PeakData we get:
DataPosition = cell2mat( arrayfun(@(c) c.Position(:,:), PeakData(:), 'Uniform', 0) );
DataIndex = cell2mat( arrayfun(@(c) c.DataIndex(:,:), PeakData(:), 'Uniform', 0) );

%% Get again the plot with the points where we extracted the peaks
% AllDataFig = plot(EnergyTraceTX15Bytes.Time, EnergyTraceTX15Bytes.Current);
%  hold on;
% scatter(DataPosition(:,1), DataPosition(:,2), 'r', 'filled');

%% Find the indexes in the Data where the TX starts and ends
%# And then integrate the (dis)charge in those periods
%# Finally, determine the average of the discharge periods
DataIndex = sort(DataIndex);

Discharge = zeros(size(DataIndex, 1)/2, 1);
counter = 1;

for i = 1:2:size(DataIndex, 1)
    Time = EnergyTraceTX10Bytes.Time(DataIndex(i):DataIndex(i+1)) * 1E-9;
    Current = EnergyTraceTX10Bytes.Current(DataIndex(i):DataIndex(i+1)) * 1E-9;
    
    Discharge(counter) = trapz(Time, Current); 
    counter = counter + 1;
end

AvgDischarge = mean(Discharge); % in Coulombs