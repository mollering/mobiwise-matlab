
close all;
clearvars -except EnergyTraceTX5Bytes

%% Plot data as is
figure
AllDataFig = plot(EnergyTraceTX5Bytes.Time, EnergyTraceTX5Bytes.Current);
hold on;

%% Parameters
MaxTime = EnergyTraceTX5Bytes.Time(end);      % Trace End Time
TimeResolution = EnergyTraceTX5Bytes.Time(2); % Time Resolution of the Samples 

PeakInterval = 0.1E11;                     % Time Interval between Peaks (10secs)
FirstPeakStart = 5.9148e+10;               % Start Time of the First Peak Window (ns)
FirstPeakEnd = FirstPeakStart + 16000000;  % End Time of the First Peak Window (ns)
Drift = 500000;                            % Time Drift along time (ns)

ValidThreshold = 2E7;                      % To be a valid peak, the points inside the considered time window must have above this value in Current (nA)                   

%% Filter out everything that we don't want

counter = 1;
CurrentWindowStart = FirstPeakStart;
CurrentWindowEnd = FirstPeakEnd;

% select all the windows before reaching the end
while (CurrentWindowEnd <= MaxTime)
    
    % save the points inside the Window in SelectedPoints
    Window = (EnergyTraceTX5Bytes.Time > (CurrentWindowStart) & EnergyTraceTX5Bytes.Time < (CurrentWindowEnd));
    SelectedPoints(counter).Time = EnergyTraceTX5Bytes.Time(Window);
    SelectedPoints(counter).Current = EnergyTraceTX5Bytes.Current(Window);
    
    % update the Window for the next iteration
    CurrentWindowStart = CurrentWindowStart + PeakInterval - Drift;
    CurrentWindowEnd = CurrentWindowEnd + PeakInterval - Drift;
    
    
    plot(SelectedPoints(counter).Time, SelectedPoints(counter).Current, 'r');
    
    % only accepts valid peaks
    if (max(SelectedPoints(counter).Current) >= ValidThreshold)
        counter = counter + 1;
    end
    
end

NumberOfWindows = counter - 1;

%% Peaks Refinement through Derivatives

ThresholdPos = 375000;
ThresholdNeg = -ThresholdPos;
NumberOfPoints = 12;


counter = 0;
for w = 1:NumberOfWindows
    
    Derivatives = diff(SelectedPoints(w).Current);
    StartPoint(w) = find(Derivatives > ThresholdPos, 1);
    EndPoint(w) = find(Derivatives < ThresholdNeg, 1, 'last') + 1;
    
    % Show the Peak inside the Time Window
    figure;
    plot(SelectedPoints(w).Time, SelectedPoints(w).Current);
    title(sprintf('Selected Window %d | Not Valid', w));
    
    % Show the Start and End Points of the Peak
    hold on;
    plot(SelectedPoints(w).Time(StartPoint(w)), SelectedPoints(w).Current(StartPoint(w)), 's');
    plot(SelectedPoints(w).Time(EndPoint(w)), SelectedPoints(w).Current(EndPoint(w)), 's');
    
    % only accpets peaks with the selected number of Points
    if ((EndPoint(w) - StartPoint(w) + 1) == NumberOfPoints)
        counter = counter + 1;
        SavedStartPoint(counter) = StartPoint(w);
        SavedEndPoint(counter) = EndPoint(w);
        title(sprintf('Selected Window %d | - Valid', w));
    end
    
end

%% Avg Discharge Calculation
NumberOfPeaks = counter;

Discharges = zeros(NumberOfPeaks,1);

for i = 1:NumberOfPeaks
    
    StTimeIndex = find(EnergyTraceTX5Bytes.Time == SelectedPoints(i).Time(SavedStartPoint(i)));
    EndTimeIndex = find(EnergyTraceTX5Bytes.Time == SelectedPoints(i).Time(SavedEndPoint(i)));
    
    Time = EnergyTraceTX5Bytes.Time(StTimeIndex:EndTimeIndex) * 1E-9;
    Current = EnergyTraceTX5Bytes.Current(StTimeIndex:EndTimeIndex) * 1E-9;
    
    Discharges(i) = trapz(Time, Current); 
    
end


figure;
boxplot(Discharges);





