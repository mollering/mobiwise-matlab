
close all;

clearvars -except EnergyTrace* SavedDischarges cursor_info

%% Plot data as is
figure
AllDataFig = plot(EnergyTraceTx40BytesSample2.Time, EnergyTraceTx40BytesSample2.Current);
hold on;

%% Parameters
MaxTime = EnergyTraceTx40BytesSample2.Time(end);      % Trace End Time
TimeResolution = EnergyTraceTx40BytesSample2.Time(2); % Time Resolution of the Samples 

PeakInterval = 0.1E11;                     % Time Interval between Peaks (10secs)
FirstPeakStart = cursor_info.Position(1);  % Start Time of the First Peak Window (ns)
FirstPeakEnd = FirstPeakStart + 16000000;  % End Time of the First Peak Window (ns)
Drift = 500000;                            % Time Drift along time (ns)

ValidThreshold = 2E7;                      % To be a valid peak, the points inside the considered time window must have above this value in Current (nA)                   

%% Filter out everything that we don't want

counter = 1;
CurrentWindowStart = FirstPeakStart;
CurrentWindowEnd = FirstPeakEnd;

% select all the windows before reaching the end
while (CurrentWindowEnd <= MaxTime)
    
    % save the points inside the Window in SelectedPoints
    Window = (EnergyTraceTx40BytesSample2.Time > (CurrentWindowStart) & EnergyTraceTx40BytesSample2.Time < (CurrentWindowEnd));
    SelectedPoints(counter).Time = EnergyTraceTx40BytesSample2.Time(Window);
    SelectedPoints(counter).Current = EnergyTraceTx40BytesSample2.Current(Window);
    
    % update the Window for the next iteration
    CurrentWindowStart = CurrentWindowStart + PeakInterval - Drift;
    CurrentWindowEnd = CurrentWindowEnd + PeakInterval - Drift;
    
    
    plot(SelectedPoints(counter).Time, SelectedPoints(counter).Current, 'r');
    
    % only accepts valid peaks
    if (max(SelectedPoints(counter).Current) >= ValidThreshold)
        counter = counter + 1;
    end
    
end

NumberOfWindows = counter - 1;

%% Peaks Refinement through Derivatives

ThresholdPos = 400000;
ThresholdNeg = -400000;%-ThresholdPos;
NumberOfPoints = 12;
NumberOfPointsAlt = 14;


counter = 0;
for w = 1:NumberOfWindows
    
    Derivatives = diff(SelectedPoints(w).Current);
    StartPoint(w) = find(Derivatives > ThresholdPos, 1);
    EndPoint(w) = find(Derivatives < ThresholdNeg, 1, 'last') + 1;
    
    % Show the Peak inside the Time Window
    figure;
    plot(SelectedPoints(w).Time, SelectedPoints(w).Current);
    title(sprintf('Selected Window %d | Not Valid', w));
    
    % Show the Start and End Points of the Peak
    hold on;
    plot(SelectedPoints(w).Time(StartPoint(w)), SelectedPoints(w).Current(StartPoint(w)), 's');
    plot(SelectedPoints(w).Time(EndPoint(w)), SelectedPoints(w).Current(EndPoint(w)), 's');
    
    % only accpets peaks with the selected number of Points
    if ((EndPoint(w) - StartPoint(w) + 1) == NumberOfPoints || ...
          (EndPoint(w) - StartPoint(w) + 1) == NumberOfPointsAlt  )
        counter = counter + 1;
        SavedStartPoint(counter) = StartPoint(w);
        SavedEndPoint(counter) = EndPoint(w);
        title(sprintf('Selected Window %d | Discharge n�%d - Valid', w, counter));
    end
    
end

%% Avg Discharge Calculation
NumberOfPeaks = counter;

Discharges = zeros(NumberOfPeaks,1);

for i = 1:NumberOfPeaks
    
    StTimeIndex = find(EnergyTraceTx40BytesSample2.Time == SelectedPoints(i).Time(SavedStartPoint(i)));
    EndTimeIndex = find(EnergyTraceTx40BytesSample2.Time == SelectedPoints(i).Time(SavedEndPoint(i)));
    
    Time = EnergyTraceTx40BytesSample2.Time(StTimeIndex:EndTimeIndex) * 1E-9;
    Current = EnergyTraceTx40BytesSample2.Current(StTimeIndex:EndTimeIndex) * 1E-9;
    
    Discharges(i) = trapz(Time, Current); 
    
end


clearvars -except EnergyTrace* SavedDischarges cursor_info Discharges
%figure;
%boxplot(Discharges);





