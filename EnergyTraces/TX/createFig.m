% Create figure
figure;

% Create axes
axes1 = axes;
hold(axes1,'on');

% Create scatter
scatter([0 1 2 3 6],AvgDischargesTX_Updated, ...
    'MarkerFaceColor',[1 0 0],'MarkerEdgeColor',[1 0 0],...
    'Marker','x',...
    'LineWidth',12);

% Create ylabel
ylabel('Avg Discharge (Coulombs)');

% Create xlabel
xlabel('Number of Transmitted Payloads');

% Create title
title('Average Discharges Per Payloads Transmitted (1 Payload = 5Bytes)');

% Uncomment the following line to preserve the X-limits of the axes
% xlim(axes1,[-0.5 3.5]);
box(axes1,'on');
% Set the remaining axes properties
set(axes1,'FontWeight','bold','XGrid','on','XTick',[0 1 2 3 6],'YGrid','on');
