BMax = 648;

HTx_pct = (HTx * 100) / BMax
PTx_pct = (PTx * 100) / BMax

HRx_pct = (HRx * 100) / BMax
PRx_pct = (PRx * 100) / BMax

Delta = 300;
V = 3.0;
SleepA = 700E-9;
SensingA = 8.52E-3;
TxA = 8.4757E-3;

SleepW = V*SleepA;
SleepJ = SleepW*Delta;
Sleep_pct = (SleepJ * 100) / BMax

SensingW = V*SensingA;
SensingJ = SensingW*Delta;
Sensing_pct = (SensingJ * 100) / BMax

TxW = V*TxA;
TxJ = TxW*Delta;
Tx_pct = (TxJ * 100) / BMax
Rx_pct = Tx_pct


CoimbraYearInsolation  = 1600E3;  % Wh/m^2/year
CoimbraDayInsolation = CoimbraYearInsolation / 365; % Wh/m^2/day
DailyEnergy = CoimbraDayInsolation * 3600; % Joules/m^2/day

ESolar = DailyEnergy / 86400; % Joules/m^2/second
EffectiveArea = 0.0025; % m^2
PVefficiency = 0.06; 
EElectrical = ESolar * EffectiveArea * PVefficiency; % Joules/second

HarvJ = EElectrical*Delta;
Harv_pct = (HarvJ * 100) / BMax

