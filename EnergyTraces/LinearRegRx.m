load('filteredResults_J.mat');

errorbar(payloads, avgrx, stddevrx.^2, 's', ...
    'MarkerSize', 8, ...
    'MarkerEdgeColor', 'red', ...
    'MarkerFaceColor', 'red');
hold on;

X = [ones(length(payloads),1) payloads];
b = X\avgrx;

y = X*b;
plot(payloads, y, '--')

% HTx = b(1) + b(2)*0
% PTx = b(1) + b(2) - HTx
