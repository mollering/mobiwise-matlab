load('filteredResults_J.mat');

x = payloads;
y = avg;

errorbar(payloads, avg, stddev, 's', ...
    'MarkerSize', 8, ...
    'MarkerEdgeColor', 'red', ...
    'MarkerFaceColor', 'red');
hold on;


b1 = x\y;

scatter(x,y);
hold on;

X = [ones(length(x),1) x];
b = X\y;

yCalc2 = X*b;
plot(x,yCalc2,'--')
%legend('Data','Slope & Intercept','Location','best');

% HTx = b(1) + b(2)*0
% PTx = b(1) + b(2) - HTx
