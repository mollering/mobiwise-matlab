
%close all;

clearvars -except EnergyTrace* SavedDischarges cursor_info

%% Plot data as is
f = figure;
AllDataFig = plot(EnergyTraceRx60BytesSample1.Time, EnergyTraceRx60BytesSample1.Current);
hold on;

%% Parameters
MaxTime = EnergyTraceRx60BytesSample1.Time(end);      % Trace End Time
TimeResolution = EnergyTraceRx60BytesSample1.Time(2); % Time Resolution of the Samples 

NumberOfPointsOfWindow = 500;
WindowInterval = 0.1E11;                     % Regular Time Interval between Peaks (10secs)
FirstWindowStart = cursor_info.Position(1);  % Start Time of the First Peak Window (ns)
FirstWindowEnd = FirstWindowStart + TimeResolution*NumberOfPointsOfWindow;  % End Time of the First Peak Window (ns)
Drift = 0;                                   % Time Drift along time (ns)

ValidThresholdMin = 1.70E7;                   % To be a valid peak, the points inside the considered time window must have above this value in Current (nA)                   
ValidThresholdMax = 1.90E7;                   % To be a valid peak, the points inside the considered time window must have below this value in Current (nA) 
%% Filter out everything that we don't want

counter = 1;
CurrentWindowStart = FirstWindowStart;
CurrentWindowEnd = FirstWindowEnd;

% select all the windows before reaching the end
while (CurrentWindowEnd <= MaxTime)
    
    % save the points inside the Window in SelectedPoints
    Window = (EnergyTraceRx60BytesSample1.Time > (CurrentWindowStart) & EnergyTraceRx60BytesSample1.Time < (CurrentWindowEnd));
    SelectedPoints(counter).Time = EnergyTraceRx60BytesSample1.Time(Window);
    SelectedPoints(counter).Current = EnergyTraceRx60BytesSample1.Current(Window);

    % only accepts valid peaks
    if (max(SelectedPoints(counter).Current) >= ValidThresholdMin && ...
          max(SelectedPoints(counter).Current) <= ValidThresholdMax  )
        plot(SelectedPoints(counter).Time, SelectedPoints(counter).Current, 'r');
        counter = counter + 1;
    end
    
    % update the Window for the next iteration
    CurrentWindowStart = CurrentWindowStart + WindowInterval + Drift;
    CurrentWindowEnd = CurrentWindowEnd + WindowInterval + Drift;
end

NumberOfWindows = counter - 1;

%% Filter out more inside the selected Windows

NumberOfPointsOfWindow = 60;

close(f);
figure;
plot(EnergyTraceRx60BytesSample1.Time, EnergyTraceRx60BytesSample1.Current);
hold on;

isValid = true(1, NumberOfWindows);
for w = 1:NumberOfWindows
    [~, maxIndex] = max(SelectedPoints(w).Current);
    windowStartIndex = maxIndex - NumberOfPointsOfWindow/2;
    windowEndIndex = maxIndex + NumberOfPointsOfWindow/2;
    if (windowStartIndex < 1 || ...
            windowEndIndex > length(SelectedPoints(w).Current) )
        isValid(w) = false;
    else
        SelectedPoints(w).Current = SelectedPoints(w).Current(windowStartIndex:windowEndIndex);
        SelectedPoints(w).Time = SelectedPoints(w).Time(windowStartIndex:windowEndIndex);
        plot(SelectedPoints(w).Time, SelectedPoints(w).Current, 'r');
    end
end

SelectedPoints = SelectedPoints(isValid);
NumberOfWindows = length(SelectedPoints);

%% Peaks Refinement through Derivatives

ThresholdPos = 500000;
ThresholdNeg = -ThresholdPos;
NumberOfPointsMin = 12;
NumberOfPointsMax = 30;


counter = 0;
for w = 1:NumberOfWindows
    
    Derivatives = diff(SelectedPoints(w).Current);
    try
    StartPoint(w) = find(Derivatives > ThresholdPos, 1);
    EndPoint(w) = find(Derivatives < ThresholdNeg, 1, 'last') + 1;
    
    % Show the Peak inside the Time Window
    figure;
    plot(SelectedPoints(w).Time, SelectedPoints(w).Current);
    title(sprintf('Selected Window %d | Not Valid', w));
    
    % Show the Start and End Points of the Peak
    hold on;
    plot(SelectedPoints(w).Time(StartPoint(w)), SelectedPoints(w).Current(StartPoint(w)), 's');
    plot(SelectedPoints(w).Time(EndPoint(w)), SelectedPoints(w).Current(EndPoint(w)), 's');
    
    % only accepts peaks with the selected number of Points
    if ((EndPoint(w) - StartPoint(w) + 1) >= NumberOfPointsMin && ...
          (EndPoint(w) - StartPoint(w) + 1) <= NumberOfPointsMax )
        counter = counter + 1;
        SavedStartPoint(counter) = StartPoint(w);
        SavedEndPoint(counter) = EndPoint(w);
        title(sprintf('Selected Window %d | Discharge n�%d - Valid', w, counter));
    end
    catch
        SelectedPoints(w).Current = [];
        SelectedPoints(w).Time = [];
    end
    
end

%% Avg Discharge Calculation
NumberOfPeaks = counter;

Discharges = zeros(NumberOfPeaks,1);

for i = 1:NumberOfPeaks
    
    StTimeIndex = find(EnergyTraceRx60BytesSample1.Time == SelectedPoints(i).Time(SavedStartPoint(i)));
    EndTimeIndex = find(EnergyTraceRx60BytesSample1.Time == SelectedPoints(i).Time(SavedEndPoint(i)));
    
    Time = EnergyTraceRx60BytesSample1.Time(StTimeIndex:EndTimeIndex) * 1E-9;
    Current = EnergyTraceRx60BytesSample1.Current(StTimeIndex:EndTimeIndex) * 1E-9;
    
    Discharges(i) = trapz(Time, Current); 
    
end


clearvars -except EnergyTrace* SavedDischarges cursor_info Discharges
%figure;
%boxplot(Discharges);





